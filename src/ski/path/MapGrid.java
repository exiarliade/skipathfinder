package ski.path;

import ski.path.finder.Position;

public class MapGrid {

	private final short[][] grid;
	
	private final short rows;
	
	private final short cols;
	
	public MapGrid(short rows, short cols) {
		this.rows = rows;
		this.cols = cols;
		this.grid = new short[rows][cols];
	}

	public short[][] getGrid() {
		return this.grid;
	}

	public short getRows() {
		return this.rows;
	}

	public short getCols() {
		return this.cols;
	}
	
	public void setHeight(short row, short col, short height) {
		this.grid[row][col] = height;
	}
	
	public short getHeight(short row, short col) {
		return this.grid[row][col];
	}
	
	public short getHeight(Position position) {
		return this.grid[position.getRow()][position.getCol()];
	}
	
}