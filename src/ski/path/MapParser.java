package ski.path;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class MapParser {
	
	private final BufferedReader br;
	
	public MapParser(String path) throws IOException {
		this.br =  new BufferedReader(new FileReader(path));
	}
	
	public MapGrid parseMapFile() throws IOException, MapParseException {
		
		MapGrid mapGrid;
		
		String line = br.readLine();
		
		String[] dimension = line.split("\\s");
		
		if (dimension.length != 2) {
			throw new MapParseException("Incorrect Map Format");
		}
		
		mapGrid = new MapGrid(parseShort(dimension[0]), parseShort(dimension[1]));
		
		for (short row = 0; row < mapGrid.getRows(); row++) {
			line = br.readLine();
			String[] heights = line.split("\\s");
			
			if (heights.length != mapGrid.getCols()) {
				throw new MapParseException("Incorrect Map Format: Map dimension does not reflect initial setting.");
			}
			
			for (short col = 0; col < mapGrid.getCols(); col++) {
				mapGrid.setHeight(row, col, parseShort(heights[col]));
			}
		}
		
		br.close();
		
		return mapGrid;
	}
	
	private static short parseShort(String num) throws MapParseException {
		try {
			short parsedValue = Short.parseShort(num);
			return parsedValue;
		}catch (NumberFormatException nfe) {
			throw new MapParseException("Incorrect Map Format: Please check if all numbers inputted are valid");
		}
	}

}
