package ski.path;

import java.util.List;

import ski.path.finder.Position;

public class MapPath implements Comparable<MapPath>{

	private final Position startPos;

	private List<Position> path;
	
	private short length;
	
	private short drop;
	
	public MapPath(Position startPos) {
		this.startPos = startPos;
	}
	
	public Position getStartPos() {
		return startPos;
	}

	public List<Position> getPath() {
		return path;
	}

	public void setPath(List<Position> path) {
		this.path = path;
	}

	public short getLength() {
		return length;
	}

	public void setLength(short length) {
		this.length = length;
	}

	public short getDrop() {
		return drop;
	}

	public void setDrop(short drop) {
		this.drop = drop;
	}

	@Override
	public int compareTo(MapPath comparedMapPath) {
		
		if (length > comparedMapPath.getLength()) {
			return 1;
		}
		else if (length < comparedMapPath.getLength()) {
			return -1;
		}
		
		if (drop > comparedMapPath.getDrop()) {
			return 1;
		}
		else if (drop < comparedMapPath.getDrop()) {
			return -1;
		}
		
		return 0;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String pathStr = "";
		for (Position pos : path) {
			pathStr += pos;
		}
		String toString = "{" + super.toString() + ": {startPos: " + startPos + "}, {length: " + length + "}, {drop: " + drop + "},\r\n" + 
				"{path: " + path + "}}";
		return toString;
	}
}
