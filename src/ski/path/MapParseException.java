package ski.path;

@SuppressWarnings("serial")
public class MapParseException extends Exception {
	public MapParseException(String reason) {
		super(reason);
	}

}
