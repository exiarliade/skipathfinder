package ski.path;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ski.path.finder.Position;
import ski.path.util.FibonacciTier;

public class MapPaths {
	
	private final Map<FibonacciTier, List<Position>> startPosMap = new HashMap<FibonacciTier, List<Position>>();
	
	private final List<MapPath> pathsTraversed = new ArrayList<MapPath>();
	
	private FibonacciTier tierLimit = FibonacciTier.NAN;
	
	private int bestPathIndex = 0;
	
	private int longestPath = 0;
	
	public void addPosition (Position pos, short height) {
		if (height > tierLimit.thresholdval()) {
			FibonacciTier tier = FibonacciTier.classifyTier(height);
			if(startPosMap.get(tier) == null) {
				startPosMap.put(tier, new ArrayList<Position>());
			}
			startPosMap.get(tier).add(pos);
		}
	}
	
	public void addPathsTraversed (MapPath mapPath) {
		pathsTraversed.add(mapPath);
		if (mapPath.compareTo(pathsTraversed.get(bestPathIndex)) > 0) {
			bestPathIndex = pathsTraversed.size() - 1;
			longestPath = mapPath.getLength();
			System.out.println("New best path: " + mapPath);
		}
	}

	public FibonacciTier getTierLimit() {
		return tierLimit;
	}

	public void setTierLimit(FibonacciTier tierLimit) {
		this.tierLimit = tierLimit;
	}

	public int getBestPathIndex() {
		return bestPathIndex;
	}

	public void setBestPathIndex(int bestPathIndex) {
		this.bestPathIndex = bestPathIndex;
	}

	public int getLongestPath() {
		return longestPath;
	}

	public void setLongestPath(int longestPath) {
		this.longestPath = longestPath;
	}

	public Map<FibonacciTier, List<Position>> getStartPosMap() {
		Map<FibonacciTier, List<Position>> startPosMap = new HashMap<FibonacciTier, List<Position>>(this.startPosMap);
		return startPosMap;
	}

	public List<MapPath> getPathsTraversed() {
		return pathsTraversed;
	}
	
}
