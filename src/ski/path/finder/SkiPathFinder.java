package ski.path.finder;

import java.io.IOException;
import java.util.List;

import ski.path.MapGrid;
import ski.path.MapParseException;
import ski.path.MapParser;
import ski.path.MapPath;
import ski.path.MapPaths;
import ski.path.util.FibonacciTier;

public class SkiPathFinder {

	public static void main(String[] args) throws IOException, MapParseException {
		// TODO Auto-generated method stub
		MapParser mapParser = new MapParser("map.txt");
		MapGrid mapGrid = mapParser.parseMapFile();
		
		FibonacciTier directPathAttemptTier = FibonacciTier.TEN;
		
		DirectionControl.mapGrid = mapGrid;
		
		MapPaths mapPaths = new MapPaths();

		for(short row = 0; row < mapGrid.getRows(); row++) {
			for(short col = 0; col < mapGrid.getCols(); col++) {
				Position position = new Position(row, col);
				short height = mapGrid.getHeight(position);
				FibonacciTier tier = FibonacciTier.classifyTier(height);
				
				if (tier.compareTo(directPathAttemptTier) >= 0) {
					MapPath mapPath = new MapPath(position);
					DirectionControl.direct(mapPath);
					mapPaths.addPathsTraversed(mapPath);
					if (mapPath.getLength() > mapPaths.getTierLimit().thresholdval()) {
						FibonacciTier updateTierLimit = FibonacciTier.classifyTier(mapPath.getLength());
						mapPaths.setTierLimit(updateTierLimit);
					}
				}else {
					mapPaths.addPosition(position, height);
				}
			}
		}
		
		findBestPath(mapPaths);
		
		printBestPath(mapGrid, mapPaths.getPathsTraversed().get(mapPaths.getBestPathIndex()));
		
	}
	
	public static void findBestPath(MapPaths mapPaths) {
		FibonacciTier[] tierList = FibonacciTier.values();
		for (int i = tierList.length - 1; i >=0; i--) {
			if (tierList[i].compareTo(mapPaths.getTierLimit())<0) {
				break;
			}
			List<Position> startPosList = mapPaths.getStartPosMap().get(tierList[i]);
			if (startPosList == null || startPosList.isEmpty()) {
				continue;
			}
			
			for (Position pos : startPosList) {
				MapPath mapPath = new MapPath(pos);
				DirectionControl.direct(mapPath);
				mapPaths.addPathsTraversed(mapPath);
				if (mapPath.getLength() > mapPaths.getTierLimit().thresholdval()) {
					FibonacciTier updateTierLimit = FibonacciTier.classifyTier(mapPath.getLength());
					mapPaths.setTierLimit(updateTierLimit);
				}
			}
		}
	}
	
	public static void printBestPath(MapGrid mapGrid, MapPath mapPath) {
		System.out.println(mapPath);
		for (Position pos : mapPath.getPath()) {
			System.out.print("[" + mapGrid.getHeight(pos) + "]");
		}
	}

}
