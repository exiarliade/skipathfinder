package ski.path.finder;

import ski.path.util.Direction;

public class Position {
	
	private final short row;
	
	private final short col;
	
	public Position(short row, short col) {
		this.row = row;
		this.col = col;
	}

	public short getRow() {
		return row;
	}

	public short getCol() {
		return col;
	}
	
	public Position move(Direction direction) {
		short verPosition = (short)(row + direction.getVerPosMovement());
		short horPosition = (short)(col + direction.getHorPosMovement());
		
		return new Position(verPosition, horPosition);
		
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String toString = "[" + row + ", " + col + "]";
		return toString;
	}
	
}
