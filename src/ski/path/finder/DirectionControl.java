package ski.path.finder;

import java.util.ArrayList;
import java.util.List;

import ski.path.MapGrid;
import ski.path.MapPath;
import ski.path.util.Direction;

public class DirectionControl {
	
	public static MapGrid mapGrid;
	
	public static void direct(MapPath mapPath) {
		List<Position> path = direct(mapPath.getStartPos(), null);
		mapPath.setPath(path);
		mapPath.setLength((short)path.size());
		
		int initialHeight = visit(Direction.STAY, mapPath.getStartPos());
		int finalHeight = visit(Direction.STAY, path.get(mapPath.getLength() - 1));
		
		mapPath.setDrop((short)(initialHeight - finalHeight));
	}
	
	public static List<Position> direct(Position startingPosition, Direction origin) {
		
		List<Direction> directionQueue = createDirectionQueue(startingPosition, origin);
		
		List<Position> probPath = null;
		
		if (directionQueue.isEmpty()) {
			probPath = new ArrayList<Position>();
			probPath.add(startingPosition);
			return probPath;
		}
		
		for (Direction direction : directionQueue) {
			List<Position> tempPath = direct(nextPosition(direction, startingPosition), Direction.opposite(direction));
			
			if (probPath == null || tempPath.size() > probPath.size()) {
				probPath = tempPath;
			}
			else if (tempPath.size() == probPath.size()){
				short startHeight = mapGrid.getHeight(startingPosition);
				short tempDrop = (short)(startHeight - mapGrid.getHeight(tempPath.get(tempPath.size() - 1)));
				short probDrop = (short)(startHeight - mapGrid.getHeight(probPath.get(probPath.size() - 1)));
				
				if (tempDrop > probDrop) {
					probPath = tempPath;
				}
			}
		}
		
		probPath.add(0, startingPosition);
		
		return probPath;
	}
	
	private static Position nextPosition(Direction direction, Position startingPosition) {
		Position nextPosition = startingPosition.move(direction);
		
		if(nextPosition.getRow() >= mapGrid.getRows() || nextPosition.getRow() < 0 
				|| nextPosition.getCol() >= mapGrid.getCols() || nextPosition.getCol() < 0) {
			return null;
		}
		
		return nextPosition;
		
	}
	
	private static short visit(Direction direction, Position startingPosition) {
		Position nextPosition = nextPosition(direction, startingPosition);
		
		if (nextPosition == null ) {
			return Short.MAX_VALUE;
		}
		
		return mapGrid.getHeight(nextPosition.getRow(), nextPosition.getCol());
	}
	
	private static List<Direction> createDirectionQueue(Position startingPosition, Direction origin) {
		short current = visit(Direction.STAY, startingPosition);
		short north = Direction.NORTH == origin ? Short.MAX_VALUE:visit(Direction.NORTH, startingPosition);
		short south = Direction.SOUTH == origin ? Short.MAX_VALUE:visit(Direction.SOUTH, startingPosition);
		short east = Direction.EAST == origin ? Short.MAX_VALUE:visit(Direction.EAST, startingPosition);
		short west = Direction.WEST == origin ? Short.MAX_VALUE:visit(Direction.WEST, startingPosition);
		
		List<Direction> directionQueue = new ArrayList<Direction>();
		short highest = -1;
		
		if(north < current) {
			directionQueue.add(Direction.NORTH);
			highest = north;
		}
		
		if (south > highest && south < current) {
			directionQueue.add(0, Direction.SOUTH);
			highest = south;
		}
		else if (south < current){
			directionQueue.add(Direction.SOUTH);
		}
		
		if (east > highest && east < current) {
			directionQueue.add(0, Direction.EAST);
			highest = east;
		}
		else if (east < current){
			directionQueue.add(Direction.EAST);
		}
		
		if (west > highest && west < current) {
			directionQueue.add(0, Direction.WEST);
			highest = west;
		}
		else if (west < current){
			directionQueue.add(Direction.WEST);
		}
		
		return directionQueue;
	}
}
