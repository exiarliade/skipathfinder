package ski.path.util;

public enum FibonacciTier implements Comparable<FibonacciTier>{

	NAN		("NAN", 0),
	ZERO	("0", 8),
	ONE		("1", 13),
	TWO		("2", 21),
	THREE	("3", 34),
	FOUR	("4", 55),
	FIVE	("5", 89),
	SIX		("6", 144),
	SEVEN	("7", 233),
	EIGHT	("8", 377),
	NINE	("9", 610),
	TEN		("10", 987);
	
	private FibonacciTier(String tier, int thresholdval) {
		this.tier = tier;
		this.thresholdval = thresholdval;
	}
	
	private String tier;
	
	private int thresholdval;
	
	public String tier() {
		return tier;
	}
	
	public int thresholdval() {
		return thresholdval;
	}
	
	public static FibonacciTier classifyTier(int height) {
		FibonacciTier tier = FibonacciTier.NAN;
		if (height > FibonacciTier.TEN.thresholdval()) {
			tier = FibonacciTier.TEN;
		}
		else if (height > FibonacciTier.NINE.thresholdval()) {
			tier = FibonacciTier.NINE;
		}
		else if (height > FibonacciTier.EIGHT.thresholdval()) {
			tier = FibonacciTier.EIGHT;
		}
		else if (height > FibonacciTier.SEVEN.thresholdval()) {
			tier = FibonacciTier.SEVEN;
		}
		else if (height > FibonacciTier.SIX.thresholdval()) {
			tier = FibonacciTier.SIX;
		}
		else if (height > FibonacciTier.FIVE.thresholdval()) {
			tier = FibonacciTier.FIVE;
		}
		else if (height > FibonacciTier.FOUR.thresholdval()) {
			tier = FibonacciTier.FOUR;
		}
		else if (height > FibonacciTier.THREE.thresholdval()) {
			tier = FibonacciTier.THREE;
		}
		else if (height > FibonacciTier.TWO.thresholdval()) {
			tier = FibonacciTier.TWO;
		}
		else if (height > FibonacciTier.ONE.thresholdval()) {
			tier = FibonacciTier.ONE;
		}
		
		return tier;
	}
}
