package ski.path.util;

public enum Direction {

	STAY	(0, 0),
	NORTH	(-1, 0),
	SOUTH	(1, 0),
	EAST	(0, 1),
	WEST	(0, -1);
	
	private final int verPosMovement;
	
	private final int horPosMovement;
	
	private Direction (int verPosMovement, int horPosMovement) {
		this.verPosMovement = verPosMovement;
		this.horPosMovement = horPosMovement;
	}

	public int getVerPosMovement() {
		return verPosMovement;
	}

	public int getHorPosMovement() {
		return horPosMovement;
	}
	
	public static Direction opposite(Direction dir) {
		switch(dir) {
			case NORTH:
				return SOUTH;
			case SOUTH:
				return NORTH;
			case EAST:
				return WEST;
			case WEST:
				return EAST;
			default:
				return STAY;
		}
	}
}
